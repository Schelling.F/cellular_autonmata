"""
Config:
Thise file holdes parameters for the cellular automa simulation
"""

WINDOW_SIZE = [1500,1000]
ENV_SIZE = [1100,600]#[1300,800]
ENV_COLOR = [32,32,32]
DOT_SIZE = 5
GAP_SIZE = 0.5
DOT_COLOR = [200,255,255]
FADE_INTERVALL = 20
