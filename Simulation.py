
import arcade
import Config
from Cell import Cell
from Cell import Map
import itertools
import multiprocessing
import time


class Cellular_Automota_Simulation(arcade.Window):
    """ Main application class. """

    def __init__(self):
        super().__init__(Config.WINDOW_SIZE[0], Config.WINDOW_SIZE[1], 'CELLULAR AUTONOMA SIMULATION')
        self.start_sim()

    def start_sim(self,paused = True):
        self.paused = paused
        self.cells = [Cell(pos) for pos in self.make_cells()]
        self.map = Map([self.map_x,self.map_y],self.cells)
        self.pos_delta_x = int((Config.WINDOW_SIZE[0]-Config.ENV_SIZE[0])/2)
        self.pos_delta_y = int((Config.WINDOW_SIZE[1]-Config.ENV_SIZE[1])/2)
        self.epoch = 0
    
    def make_cells(self):
        mk_list = (lambda x: list(range(int((Config.DOT_SIZE)), int(Config.ENV_SIZE[x] - (Config.DOT_SIZE/2)),int((Config.DOT_SIZE*2)+Config.GAP_SIZE))))
        x_list = mk_list(0)
        y_list = mk_list(1)
        self.map_x, self.map_y = len(y_list),len(x_list)
        pos_list = []
        for x_num,x in enumerate(x_list):
            for y_num,y in enumerate(y_list):
                pos_list.append([[x,y],[x_num,y_num]])
        return pos_list

    def setup(self):
        # --- Create the vertex buffers objects for each square before we do
        # any drawing.
        environment_shapes = arcade.ShapeElementList()
        env = arcade.create_rectangle_filled(Config.WINDOW_SIZE[0]/2,Config.WINDOW_SIZE[1]/2,Config.ENV_SIZE[0],Config.ENV_SIZE[1], color = Config.ENV_COLOR)
        environment_shapes.append(env)        
        return environment_shapes

    def draw_text(self):
        """
        Draws all Text on to Window 
        """
        epoch = "Epoch: {}".format(self.epoch//Config.FADE_INTERVALL)
        arcade.draw_text(epoch,0, 20 , arcade.color.WHITE, 12,align='center',width=Config.WINDOW_SIZE[0])
        paused = "simultion paused: {} || press Enter to toggle || press R to restart".format(self.paused)
        arcade.draw_text(paused,0, 50 , arcade.color.WHITE, 10,align='center',width=Config.WINDOW_SIZE[0])
        output_heading = "Cellular Autonoma"
        arcade.draw_text(output_heading, 0, Config.WINDOW_SIZE[1]-30 , arcade.color.WHITE, 25,align='center',width=Config.WINDOW_SIZE[0])
        output_name = "By Felix Schelling"
        arcade.draw_text(output_name, 0, Config.WINDOW_SIZE[1]-50 , arcade.color.WHITE, 10,align='center',width=Config.WINDOW_SIZE[0])
        return

    def draw_cell(self,cell):
        if cell.state or cell.prev_state:
            arcade.draw_circle_filled(cell.render_pos[0]+self.pos_delta_x,cell.render_pos[1]+self.pos_delta_y,cell.size,cell.get_color(self.epoch),)

    def on_draw(self):
        """
        Render the screen.
        """
        start = time.time()
        arcade.start_render()
        self.setup().draw()
        for cell in self.cells:
            self.draw_cell(cell)
        self.draw_text()

    def on_update(self, delta_time):
        start = time.time()
        if self.paused == False: 
            if self.epoch % Config.FADE_INTERVALL == (Config.FADE_INTERVALL-1):
                for cell in self.cells:
                    state,pos = cell.step(self.map.global_state)
                    self.map.global_state[pos[1], pos[0]] = state
            self.epoch += 1

    def on_key_press(self, symbol, modifiers):
        """
        Handle user keyboard input
        Q: Quit the game
        P: Pause the game
        """
        if symbol == arcade.key.Q:
            # Quit immediately
            print('Simulation finished')
            print('-'*80)
            arcade.close_window()
        if symbol == arcade.key.ENTER:
            self.paused = not self.paused
        if symbol == arcade.key.R:
            self.start_sim(paused=False)




def main():
    Cellular_Automota_Simulation()
    arcade.run()


if __name__ == "__main__":
    main()