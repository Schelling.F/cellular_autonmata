# Cellular Automata

**Author:** Felix Schelling <br>
**Date:** 11.10.20


Thise Repro represents some relaxed Sunday coding. Its an Implementation of 2D cellular automata with a conways game of life rules set.
The Simulation envionment was built using Arcade. The Cell(s) it self are pretty straight forward. The Simulatioon can be configured using in the Config File.
The Simulation acts as a main. 
***
Here's a sample video:

![Sample Video](cellular_automata.mp4)

