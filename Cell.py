
import Config 
import random
import numpy as np
class Live():

    def stay_alive(self, neighbours):
        if neighbours in [2,3]:
            return True
        else:
            return False
    
    def stay_dead(self, neighbours):
        if neighbours == 3:
            return True
        else:
            return False

    def apply_rules(self, neighbours, alive):
        if alive:
            return self.stay_alive(neighbours)
        else:
            return self.stay_dead(neighbours)


class Cell():

    def __init__(self, pos):
        self.pos = pos[1]
        self.render_pos = pos[0]
        self.size = Config.DOT_SIZE
        self.state =  random.gauss(0.25,0.2) > 0.52
        self.prev_state = self.state
        self.color_dict = {True:np.array(Config.DOT_COLOR),False:np.array(Config.ENV_COLOR)}
        self.Rules = Live()

    def get_color(self,epoch):
        t = epoch % Config.FADE_INTERVALL 
        delta = self.color_dict[self.state]-self.color_dict[self.prev_state]
        color = self.color_dict[self.prev_state] + delta*(t/Config.FADE_INTERVALL)
        return list(color)

    def step(self, state_map):
        alives = np.sum(state_map[max(0,self.pos[1]-1):min(self.pos[1]+2,len(state_map[1])),max(0,self.pos[0]-1):min(self.pos[0]+2,len(state_map[0]))])-int(self.state)
        self.prev_state = self.state
        self.state = self.Rules.apply_rules(alives, self.state)
        return self.state, self.pos


class Map():
    def __init__(self,size,cells):
        self.global_state = np.zeros(size)
        self.update(cells)
    
    def update(self,cells):
        for cell in cells:
            self.global_state[cell.pos[1],cell.pos[0]] = cell.state

    
if __name__ == "__main__":
    cell = Cell(None)
 